# -*- coding: utf-8 -*-
"""
.. module:: messages.status
   :platform: Linux, Windows, OSX
   :synopsis: Module defining status message classes.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of message classes used by MinecraftBridge.  Classes in this module
are related to status messages.
"""