# -*- coding: utf-8 -*-
"""
.. module:: ihmc_ta2_dyad
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating IHMC Dyad measures

.. moduleauthor:: Noel Chen <yunhsua3@andrew.cmu.edu>

Definition of a class encapsulating Aptima TA3 measures.
"""

import json
import enum

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

from collections import namedtuple


class TA3Measures(BaseMessage):
    """
    A class encapsulating TA3 measure messages.

    Attributes
    ----------
    study_version : int
        the current study
    elapsed_milliseconds : int
        the current elapsed milliseconds
    event_properties : TA3Measures.EventProperties
        data which describes the event which triggers the measure.
    measure_data : TA3Measures.Measuredata
        Probability that the participants are in a dyad
    """

    class QualifyingEventType(enum.Enum):
        """
        Enumeration of possible qualifying event types in event_properties
        """
        event = "event"
        timedEvent = "timedEvent"
        time = "time"

    # Definition of lightweight class to store event_properties information
    EventProperties = namedtuple("EventProperties", 
                            ["qualifying_event_sub_type", 
                            "qualifying_event_type", 
                            "qualifying_event_message_type"])

    # Definition of lightweight class to store event_properties information
    MeasureData = namedtuple("MeasureData", 
                            ["description", "datatype", 
                            "measure_id", "measure_value", 
                            "additional_data"])


    def __init__(self, **kwargs):

        BaseMessage.__init__(self, **kwargs)

        # Check to see if the necessary arguments have been passed, raise an 
        # exception if one is missing
        for arg_name in ["study_version", "elapsed_milliseconds",
                         "event_properties", 
                         "measure_data"]:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(str(self), 
                                                      arg_name) from None

        # Make sure that `event_properties` are correctly formatted
        event_properties = kwargs.get("event_properties", [])
        for arg_name in ["qualifying_event_sub_type", "qualifying_event_type",
                         "qualifying_event_message_type"]:
            if not arg_name in event_properties:
                raise MalformedMessageCreationException(str(self), "event_properties",
                                                        kwargs["event_properties"]) from None

        # Make sure that `measure_data` are correctly formatted
        for measure_data in kwargs.get("measure_data", []):
            for arg_name in ["description", "datatype", "measure_id",
                            "measure_value", "additional_data"]:
                if not arg_name in measure_data:
                    raise MalformedMessageCreationException(str(self), "measure_data",
                                                            kwargs["measure_data"]) from None

        # Populate the fields
        self._study_version = kwargs["study_version"]
        self._elapsed_milliseconds = kwargs["elapsed_milliseconds"]
        self._event_properties = TA3Measures.EventProperties(
            TA3Measures.QualifyingEventType[event_properties["qualifying_event_type"]],
            event_properties["qualifying_event_sub_type"],
            event_properties["qualifying_event_message_type"])
        self._measure_data = TA3Measures.MeasureData(
            measure_data["description"],
            measure_data["datatype"],
            measure_data["measure_id"],
            measure_data["measure_value"],
            measure_data["additional_data"],
        )


    @property
    def study_version(self):
        """

        Attempting to set `study_version` raises an `ImmutableAttributeException`
        """

        return self._study_version

    @study_version.setter
    def study_version(self, _):

        raise ImmutableAttributeException(self, "study_version")


    @property
    def elapsed_milliseconds(self):
        """

        Attempting to set `elapsed_milliseconds` raises an `ImmutableAttributeException`
        """

        return self._elapsed_milliseconds

    @elapsed_milliseconds.setter
    def elapsed_milliseconds(self, _):

        raise ImmutableAttributeException(self, "elapsed_milliseconds")


    @property
    def event_properties(self):
        """

        Attempting to set `event_properties` raises an `ImmutableAttributeException`
        """

        return self._event_properties

    @event_properties.setter
    def event_properties(self, _):

        raise ImmutableAttributeException(self, "event_properties")


    @property
    def measure_data(self):
        """

        Attempting to set `measure_data` raises an `ImmutableAttributeException`
        """

        return self._measure_data

    @measure_data.setter
    def measure_data(self, _):

        raise ImmutableAttributeException(self, "measure_data")


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'TA3Measures')
        """

        return self.__class__.__name__


    def toDict(self):
        """
        Generates a dictionary representation of the TA3Measures message.
        Message information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the TA3Measures message.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the Cognitive Load data
        jsonDict["data"]["study_version"] = self.study_version
        jsonDict["data"]["elapsed_milliseconds"] = self.elapsed_milliseconds
        jsonDict["data"]["event_properties"] = {
            "qualifying_event_type": self.event_properties.qualifying_event_type,
            "qualifying_event_sub_type": self.event_properties.qualifying_event_sub_type,
            "qualifying_event_message_type": self.event_properties.qualifying_event_message_type,
        }
        jsonDict["data"]["measure_data"] = { 
            "description": self.measure_data.description,
            "datatype": self.measure_data.datatype,
            "measure_id": self.measure_data.measure_id,
            "measure_value": self.measure_data.measure_value,
            "additional_data": self.measure_data.additional_data,
        }

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the TA3Measures message.  
        Message information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            TA3Measures message.
        """

        return json.dumps(self.toDict())
