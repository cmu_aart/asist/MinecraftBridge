# -*- coding: utf-8 -*-
"""
.. module:: gallup_ta2_gelp
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Emergent Leadership Prediction Messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Emergent Leadership Prediction messages.
"""

import json

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage



class GELP_Result:
    """
    A class for representing a single GELP result for an individual player.

    Attributes
    ----------
    participant_id
    callsign
    gelp_components
    gelp_components_lower_bound
    gelp_components_upper_bound
    gelp_missingness_factor
    gelp_overall
    gelp_overall_lower_bound
    gelp_overall_upper_bound
    """

    def __init__(self, **kwargs):
        self._attributes = [
            'participant_id', 'callsign',
            'gelp_components', 'gelp_components_lower_bound', 'gelp_components_upper_bound',
            'gelp_missingness_factor',
            'gelp_overall', 'gelp_overall_lower_bound', 'gelp_overall_upper_bound',
        ]
        for attr in self._attributes:
             if not attr in kwargs:
                raise MissingMessageArgumentException(str(self), attr) from None

        self._participant_id = kwargs["participant_id"]
        self._callsign = kwargs["callsign"]
        self._gelp_components = kwargs["gelp_components"]
        self._gelp_components_lower_bound = kwargs["gelp_components_lower_bound"]
        self._gelp_components_upper_bound = kwargs["gelp_components_upper_bound"]
        self._gelp_missingness_factor = kwargs["gelp_missingness_factor"]
        self._gelp_overall = kwargs["gelp_overall"]
        self._gelp_overall_lower_bound = kwargs["gelp_overall_lower_bound"]
        self._gelp_overall_upper_bound = kwargs["gelp_overall_upper_bound"]

    @property
    def participant_id(self):
        return self._participant_id

    @property
    def callsign(self):
        return self._callsign

    @property
    def gelp_components(self):
        return self._gelp_components

    @property
    def gelp_components_lower_bound(self):
        return self._gelp_components_lower_bound

    @property
    def gelp_components_upper_bound(self):
        return self._gelp_components_upper_bound

    @property
    def gelp_missingness_factor(self):
        return self._gelp_missingness_factor

    @property
    def gelp_overall(self):
        return self._gelp_overall

    @property
    def gelp_overall_lower_bound(self):
        return self._gelp_overall_lower_bound

    @property
    def gelp_overall_upper_bound(self):
        return self._gelp_overall_lower_bound

    def toDict(self):
        return {attr: getattr(self, attr) for attr in self._attributes}



class GELP(BaseMessage):
    """
    A class encapsulating emergent leadership prediction messages.

    Attributes
    ----------
    created_ts : str
        Timestamp for when the GELP message was published
    gelp_msg_id : str
        Unique ID for the GELP message
    gelp_pub_minute : int
        The number of minutes into the mission the GELP message published
    gelp_results : list of `GELP_Result`
        List containing a `GELP_Result` for each participant
    minute : int
        Alias for `gelp_pub_minute`
    results : list of `GELP_Result`
        Alias for `gelp_results`
    """

    def __init__(self, **kwargs):

        BaseMessage.__init__(self, **kwargs)

        # Check to see if the necessary arguments have been passed, raise an 
        # exception if one is missing
        for arg_name in ['created_ts', 'gelp_pub_minute', 'gelp_results', 'gelp_msg_id']:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(str(self), 
                                                      arg_name) from None

        self._created_ts = kwargs["created_ts"]
        self._gelp_msg_id = kwargs["gelp_msg_id"]
        self._gelp_pub_minute = kwargs["gelp_pub_minute"]
        self._gelp_results = [GELP_Result(**result) for result in kwargs["gelp_results"]]


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'AgentFeedback')
        """
    
        return self.__class__.__name__


    @property
    def created_ts(self):
        """
    
        Attempting to set `created_ts` raises an `ImmutableAttributeException`
        """
        return self._created_ts
    
    @created_ts.setter
    def created_ts(self, _):
        raise ImmutableAttributeException(str(self), "created_ts")
    

    @property
    def gelp_msg_id(self):
        """
    
        Attempting to set `gelp_msg_id` raises an `ImmutableAttributeException`
        """
        return self._gelp_msg_id
    
    @gelp_msg_id.setter
    def gelp_msg_id(self, _):
        raise ImmutableAttributeException(str(self), "gelp_msg_id")
    

    @property
    def gelp_pub_minute(self):
        """
    
        Attempting to set `gelp_pub_minute` raises an `ImmutableAttributeException`
        """
        return self._gelp_pub_minute
    
    @gelp_pub_minute.setter
    def gelp_pub_minute(self, _):
        raise ImmutableAttributeException(str(self), "gelp_pub_minute")
    

    @property
    def gelp_results(self):
        """
    
        Attempting to set `gelp_results` raises an `ImmutableAttributeException`
        """
        return self._gelp_results
    
    @gelp_results.setter
    def gelp_results(self, _):
        raise ImmutableAttributeException(str(self), "gelp_results")
    

    @property
    def results(self):
        """
    
        Attempting to set `results` raises an `ImmutableAttributeException`
        """
        return self._gelp_results
    
    @gelp_results.setter
    def results(self, _):
        raise ImmutableAttributeException(str(self), "results")


    @property
    def minute(self):
        """
    
        Attempting to set `minute` raises an `ImmutableAttributeException`
        """
        return self._gelp_pub_minute
    
    @minute.setter
    def minute(self, _):
        raise ImmutableAttributeException(str(self), "minute")


    def toDict(self):
        """
        Generates a dictionary representation of the GELP message.
        Message information is contained in a dictionary under the key "data".
        Additional named headers may also be present.
    
        Returns
        -------
        dict
            A dictionary representation of the ##### message.
        """

        jsonDict = BaseMessage.toDict(self, False)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the GELP results data
        jsonDict["data"]["created_ts"] = self.created_ts
        jsonDict["data"]["gelp_msg_id"] = self.gelp_msg_id
        jsonDict["data"]["gelp_pub_minute"] = self.gelp_pub_minute
        jsonDict["data"]["gelp_results"] = [gelp_result.toDict() for gelp_result in self._gelp_results]

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the GELP message.  
        Message information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            GELP message.
        """

        return json.dumps(self.toDict())
