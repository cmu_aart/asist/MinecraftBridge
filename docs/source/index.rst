.. MinecraftBridge documentation master file, created by
   sphinx-quickstart on Mon Jul  5 21:53:25 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MinecraftBridge's documentation!
===========================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:

.. include:: license.rst
.. include:: overview.rst
.. include:: installation.rst
.. include:: architecture.rst
.. include:: usage.rst
.. include:: messages.rst
.. include:: bridges.rst
.. include:: code.rst
.. include:: changelog.rst

Indices and tables
==================

